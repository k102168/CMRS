var express = require('express');
var mongoose = require('mongoose');
var port = 3000;
var morgan = require('morgan');
var bodyParser = require('body-parser');
var app = express();
var path = require('path');

var CMRS = require('./app/routes/CMRS')();

var options = { server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }, 
                replset: { socketOptions: { keepAlive: 1, connectTimeoutMS : 30000 } } }; 
mongoose.connect('mongodb://k102168@nu.edu.pk:18527rogue@ds145385.mlab.com:45385/audio_files', options);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

//Log with Morgan
app.use(morgan('dev'));
//parse application/json and look for raw text                                        
app.use(bodyParser.json());                                     
app.use(bodyParser.urlencoded({extended: true}));               
app.use(bodyParser.text());                                    
app.use(bodyParser.json({ type: 'application/json'}));  

//Static files
app.use(express.static(__dirname + '/public')); 

app.route('/CMRS')
	.post(CMRS.post)
	.get(CMRS.getAll);
app.route('/CMRS/:id')
	.get(CMRS.getOne);

app.listen(port);
console.log('listening on port ' + port);