//Main file
var app = angular.module('CMRSApp', ['addCMRSCtrl', 'galleryCtrl','detailCtrl', 'ngRoute', 'angular-filepicker'])
    .config(function($routeProvider, filepickerProvider){
		//The route provider handles the client request to switch route
        $routeProvider.when('/addCMRS', { 			
            templateUrl: 'partials/addCMRS.html',
			controller: 'addCMRSController'			
        })
		.when('/gallery', {
            templateUrl: 'partials/gallery.html',
			controller: 'galleryController'
        })
		.when('/detail/:id', {
            templateUrl: 'partials/detail.html',
			controller: 'detailController'
        })
		//Redirect to addCMRS in all the other cases.
		.otherwise({redirectTo:'/addCMRS'});
		//Add the API key to use filestack service
        filepickerProvider.setKey('A1RFtQLnBRFKVa9gDsbuoz');
});