var galleryCtrl = angular.module('galleryCtrl', []);
galleryCtrl.controller('galleryController', function($scope, $http){
	$scope.CMRSes = [];
	//Retrieve all the CMRSes to show the gallery
	$http.get('/CMRS')
		.success(function(data){
			console.log(JSON.stringify(data));
			$scope.CMRSes = data;
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});
	
});