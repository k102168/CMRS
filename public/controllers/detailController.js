var detailCtrl = angular.module('detailCtrl', []);
detailCtrl.controller('detailController', function($scope, $http, $routeParams){
	$scope.CMRS = {};
	//get the id to query the db and retrieve the correct CMRS
	var id = $routeParams.id;
	$http.get('/CMRS/' + id)
		.success(function(data){
			console.log(JSON.stringify(data));
			$scope.CMRS = data;
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});		
});