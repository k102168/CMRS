var addCtrl = angular.module('addCMRSCtrl', []);
addCtrl.controller('addCMRSController', function($scope, $http, filepickerService){
	$scope.CMRS = {};
	//Send the newly created CMRS to the server to store in the db
	$scope.createCMRS = function(){
		$http.post('/CMRS', $scope.CMRS)
			.success(function(data){
				console.log(JSON.stringify(data));
				//Clean the form to allow the user to create new CMRSes
				$scope.CMRS = {};
			})
			.error(function(data) {
                console.log('Error: ' + data);
            });
	};
	//Single file upload, you can take a look at the options

	$scope.upload = function(){
		console.log("test clear upload");

		filepickerService.pick(
				{
					mimetype: 'image/*',
					language: 'en',
					services: ['COMPUTER','DROPBOX','GOOGLE_DRIVE','IMAGE_SEARCH', 'FACEBOOK', 'INSTAGRAM'],
					customSourcePath: 'C:\Users\Owais Ahmed\Desktop\CMRSMEAN\public\partials'
				},
				function(Blob){
					console.log(JSON.stringify(Blob));
					$scope.CMRS.audio = Blob;
					$scope.$apply();
				}
		);
	};
	//Multiple files upload set to 3 as max number
	$scope.uploadMultiple = function(){
		filepickerService.pickMultiple(
            {
				mimetype: 'image/*',
				language: 'en',
				maxFiles: 3,
				services: ['COMPUTER','DROPBOX','GOOGLE_DRIVE','IMAGE_SEARCH', 'FACEBOOK', 'INSTAGRAM'],
				openTo: 'IMAGE_SEARCH'
			},
            function(Blob){
				console.log(JSON.stringify(Blob));
				$scope.CMRS.moreAudio = Blob;
				$scope.$apply();
			}
        );
	};	
});