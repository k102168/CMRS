# CMRS Web Application

This project is generated with MEAN Stack 
[Mongodb](https://www.mongodb.com/)
[Expressjs](https://expressjs.com/)
[Angularjs](https://angularjs.org/)
[Nodejs](https://nodejs.org/en/)

version 0.15.1.

## Pre-Installation 

To run this application in your local environment you need to install certain technologies:

1: install [Nodejs](https://nodejs.org/en/)

2: install [git](https://nodejs.org/en/) to clone or further development you can also download via zip (optional)

3: install [Mongodb](https://www.mongodb.com/) 

4: Create account on [mlab](https://mlab.com/) for schema and Database connection.

5: Create account on [filestack](crunchify.com) for handing audio files the free account provide limited space. 


To run CMRS application First clone it

`git clone https://gitlab.com/k102168/CMRS.git`

then install dependencies 

`npm install` for node modules and `bower install` for packages dependencies

after cloning go to server.js into the root folder and enter your credentials of mongodb to connect with database

## Run locally 

run `node server` in cmd make sure your cmd path that should be the project path cmd log out with message 

go to and web browser run localhost:3000 application will be successfully running 

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
