//Dependencies
var mongoose        = require('mongoose');
var CMRS       = require('../models/CMRS');
//App routes
module.exports = function() {
	return {
		/*
		 * Get route to retrieve all the CMRSes.
		 */
		getAll : function(req, res){
			//Query the DB and if no errors, send all the CMRSes
			var query = CMRS.find({});
			query.exec(function(err, CMRSes){
				if(err) res.send(err);
				//If no errors, send them back to the client
				res.json(CMRSes);
			});
		},
		/*
		 * Post route to save a new CMRS into the DB.
		 */
		post: function(req, res){
			//Creates a new CMRS
			var newCMRS = new CMRS(req.body);
			//Save it into the DB.
			newCMRS.save(function(err){
				if(err) res.send(err);
				//If no errors, send it back to the client
				res.json(req.body);
			});
		},
		/*
		 * Get a single CMRS based on id.
		 */
		getOne: function(req, res){
			CMRS.findById(req.params.id, function(err, CMRS){
				if(err) res.send(err);
				//If no errors, send it back to the client
				res.json(CMRS);
			});		
		}
	}
};  