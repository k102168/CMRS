//Dependencies
var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

//Defines the CMRS schema
var CMRSSchema = new Schema({
    name: {type: String, required: true},
    gender: {type: String, required: true},
    superPowers: {type: String, required: true},
    audio: {type: Schema.Types.Mixed, required: true},
    moreAudio: Schema.Types.Mixed,
    createdAt: {type: Date, default: Date.now},    
});

// Sets the createdAt parameter equal to the current time
CMRSSchema.pre('save', function(next){
    now = new Date();
    if(!this.createdAt) {
        this.createdAt = now;
    }
    next();
});

//Exports the CMRSSchema for use elsewhere.
module.exports = mongoose.model('CMRS', CMRSSchema);